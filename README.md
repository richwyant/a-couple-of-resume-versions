# a-couple-of-resume-versions

## Senior Software Test Engineer 
Richard Wyant<br>
[507-271-7508](tel:507-271-7508)<br>
[richard.wyant@protonmail.com](mailto:richard.wyant@protonmail.com)<br>
[www.linkedin.com/in/richwyant](www.linkedin.com/in/richwyant)<br>

Seasoned veteran with experience improving and establishing automated software testing.

Uploading versions of my resumes here for ease of sharing and updating.

You'll find
- main resume, .txt format
- main resume, .docx format
- volunteer resume, .txt format
- volunteer resume, .docx format

## Availability
As of 30Jun24, I am available for full-time test engineering work.

Thank you for checking me out. If I can help, please feel free to reach me.