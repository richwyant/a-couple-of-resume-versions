Volunteer Work
Richard Wyant
507-271-7508
richard.wyant@protonmail.com
Rochester, MN

When possible, I engage in non-career volunteer work. I keep this separate from my primary resume as the experiences and skills relate to different audiences. This makes it clearer to communicate what I can bring to the table for each when a volunteer position requires more scrutiny before contributing.

SKILLS: Food Preparation, Safety & Security, Host, Janitorial, Driver, Lawn Care

VOLUNTEER EXPERIENCE

Resurrection and Life Lutheran Church: Jan 2021 – present
––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
• Currently serving as transport for members unable to drive.

• Served as usher. Welcomed guests and congregants. Distributed worship materials, collected offerings, and recorded attendance numbers.

• Served as kitchen staff. Researched how to best clean coffee equipment to ease other staff with dexterity issues. Included janitorial duties, busing tables, and tear down of banquet equipment.

• Served on a landscaping team. Performed lawn mowing, ditch vegetation removal, tree removal, and sod laying. 

Compassion Counseling Center: Mar 2023 – Jun 2024
––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
CCC provides low-cost temporary mental health and marriage counseling. They operate out of a church once a week. During that time, it is considered clinical space and must abide by HIPPA regulations.

• Served on a security team. Kept worship team staff and clients in appropriate areas. Protected church assets, equipment, and staff. Kept a visible presence to deter potential outbursts by troubled clients.

